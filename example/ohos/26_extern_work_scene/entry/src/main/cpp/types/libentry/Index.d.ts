/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class JSBind {
  static bindFunction: (name: string, func:Function) => Number;
  static unbindFunction: (name: string) => Number;
}

export class Message {
  message: string
  length: number

  constructor(str : string);
}

export const CallJsFunc: (name: string) => string;
export const AsyncTaskMessageReturnMessage: (message: Message, info: string) => Promise<Message>;

export class NapiTest {
  requestMsg: string
  responseMsg: string
  buffer:ArrayBuffer

  constructor();
  SetMsg : (msg : string) => void;
  GetMsg:() =>string;
  StartGetData:(args:number, msg:string, func:(args:number, msg:string) => string) => void;
}
