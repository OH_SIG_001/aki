/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include "hilog/log.h"
#include "napi/native_api.h"
#include "aki/jsbind.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"
#include "testResources/akivalue_callmethod_cmp.h"

namespace AKITEST {

#define TEST_COUNT 100000

void NapiJsonParse(napi_env env, napi_value json)
{
    napi_value jsonParse;
    napi_value str, result;
    napi_status status = napi_ok;
    const char *jsonstr = "{\"name\":\"aki\"}";
    napi_get_named_property(env, json, "parse", &jsonParse);
    napi_create_string_utf8(env, jsonstr, strlen(jsonstr), &str);
    status = napi_call_function(env, json, jsonParse, 1, &str, &result);
    if (status != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiJsonParse", "NapiJsonParse failed");
    }
}

void AkiValueCallMethod::NapiCallMethodTest(void)
{
    napi_value global;
    napi_value result;
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_global(env, &global);
    napi_get_named_property(env, global, "JSON", &result);
    AKITEST::TestTime().NapiTestTime(TEST_COUNT, "Value", "CallMethod", NapiJsonParse, env, result);
}

void AkiJsonParse(aki::Value &value)
{
    value.CallMethod("parse", "{\"name\":\"aki\"}");
}

void AkiValueCallMethod::AkiCallMethodTest(void)
{
    napi_value global;
    napi_value result = nullptr;
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_global(env, &global);
    napi_get_named_property(env, global, "JSON", &result);
    aki::Value akiValue = result;
    AKITEST::TestTime().AkiTestTime(TEST_COUNT, "Value", "CallMethod", AkiJsonParse, akiValue);
}

} // namespace AKITEST