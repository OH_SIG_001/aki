/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hilog/log.h"
#include "aki/jsbind.h"
#include "akivalue_newobject_cmp.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"

namespace AKITEST {

#define TEST_COUNT 100000

void NapiNewObject(void)
{
    napi_value obj;
    napi_status status;
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    status = napi_create_object(env, &obj);
    if (status != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiNewObject", "NapiNewObject failed");
    }
}

void AkiNewObjectCmp::NapiNewObjectTest(void)
{
    AKITEST::TestTime().NapiTestTime(TEST_COUNT, "Value", "NewObject", NapiNewObject);
}

void AkiNewObject(aki::Value &val)
{
    aki::Value::NewObject(); //newobject会在返回过程中构建aki::value，耗时会有较明显的增加
}

void AkiNewObjectCmp::AkiNewObjectTest(void)
{
    aki::Value val;
    AKITEST::TestTime().AkiTestTime(TEST_COUNT, "Value", "NewObject", AkiNewObject, val);
}

}